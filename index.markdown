---
title: (λ _ λ)
theme: I am Iris, and the lens.
---

### Posts
$partial("templates/post-list.html")$

[Older Posts](/archive.html)

-------------------------------------------------------------------------------------------

### Ramblings

[No fun manifesto](fun.html)

[Linux tools I use](linux.html)

-------------------------------------------------------------------------------------------

### About me

Hi, I'm Raphy and I like computers and maths. That's all there is to know about me.

I will sporadicaly post everything that I will feel like posting here (you guessed it, probably about computers and maths).
This is an experiment, an excuse to get me writing. I'll edit posts a lot of times after publishing.

If anything you read horrifies you, feel free to contact with the links down below and yell at me. I am very eager to know all my mistakes.

### Contact me{#contact}


<div style="display:flex; justify-content: space-between">
[Telegram](https://t.me/RaphyJake "Open my TG chat")

[email](mailto:raphy@airmail.cc "Email me")

[My Last.fm](https://www.last.fm/user/ominous_cloud) 

[My GitLab](https://gitlab.com/RaphyJake) (duh!) 

[Pictures I took](https://t.me/s/raphysdumpster) 

[Hire me](https://www.fiverr.com/share/zzDewg)
</div>