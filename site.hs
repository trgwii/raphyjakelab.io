{-# LANGUAGE OverloadedStrings #-}
import Text.Pandoc.Options


import Text.Pandoc.Templates ( compileTemplate )
import Hakyll
    ( tagsField,
      dateField,
      renderAtom,
      loadAllSnapshots,
      bodyField,
      templateBodyCompiler,
      applyAsTemplate,
      create,
      pandocCompiler,
      saveSnapshot,
      makeItem,
      listField,
      constField,
      loadAll,
      recentFirst,
      tagsRules,
      fromCapture,
      buildTags,
      relativizeUrls,
      defaultContext,
      loadAndApplyTemplate,
      setExtension,
      composeRoutes,
      gsubRoute,
      compressCssCompiler,
      copyFileCompiler,
      compile,
      idRoute,
      route,
      match,
      hakyllWith,
      defaultHakyllWriterOptions,
      defaultHakyllReaderOptions,
      pandocCompilerWith,
      defaultConfiguration,
      FeedConfiguration(..),
      Tags,
      Context,
      Configuration(destinationDirectory) )
import qualified Data.Text as T

import GHC.IO.Encoding ( setLocaleEncoding, utf8 )


import           Data.Functor.Identity          ( runIdentity )
import Data.String


--------------------------------------------------------------------------------

configuration :: Configuration
configuration = defaultConfiguration
    { destinationDirectory = "public"}

markCompiler = pandocCompilerWith defaultHakyllReaderOptions 
    defaultHakyllWriterOptions {writerTableOfContents = True,
                                writerTOCDepth = 3,
                                writerTemplate = Just tocTemplate,
                                writerHTMLMathMethod = MathJax ""}


tocTemplate = either error id . runIdentity . compileTemplate "" $ T.unlines
  [ "<div class=\"toc\"><div class=\"header\">Table of Contents</div>"
  , "$toc$"
  , "</div>"
  , "$body$"
  ]



main :: IO ()
main = do 

    setLocaleEncoding utf8
    hakyllWith configuration $ do
        match "images/*" $ do
            route   idRoute
            compile copyFileCompiler

        match "css/*" $ do
            route   idRoute
            compile compressCssCompiler

        match "writings/*" $ do
            route   $ (gsubRoute "writings/" (const "")) `composeRoutes` setExtension "html"
            compile $ markCompiler
                >>= loadAndApplyTemplate "templates/default.html" defaultContext
                >>= relativizeUrls

        tags <- buildTags "posts/*" (fromCapture "tags/*.html")

        tagsRules tags $ \tag pattern -> do
            let title = "Posts tagged \"" ++ tag ++ "\""
            route idRoute
            compile $ do
                posts <- recentFirst =<< loadAll pattern
                let ctx = constField "title" title <>
                        listField "posts" postCtx (return posts) <>
                        defaultContext

                makeItem ""
                    >>= loadAndApplyTemplate "templates/tag.html" ctx
                    >>= loadAndApplyTemplate "templates/default.html" ctx
                    >>= relativizeUrls

        match "posts/*" $ do
            route $ setExtension "html"
            compile $ markCompiler
                >>= loadAndApplyTemplate "templates/post.html"    (postCtxTags tags)
                >>= saveSnapshot "content"
                >>= loadAndApplyTemplate "templates/default.html" (postCtxTags tags)
                >>= relativizeUrls

        match "404.html" $ do
            route idRoute
            compile $ pandocCompiler
                >>= loadAndApplyTemplate "templates/default.html" defaultContext

        create ["archive.html"] $ do
            route idRoute
            compile $ do
                posts <- recentFirst =<< loadAll "posts/*"
                let archiveCtx =
                        listField "posts" postCtx (return posts) <>
                        constField "theme" "We mustn't linger. It is easy to get lost in memories" <>
                        constField "title" "Archives" <>
                        defaultContext

                makeItem ""
                    >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                    >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                    >>= relativizeUrls

        match "index.markdown" $ do
            route $ setExtension "html"

            compile $ do
                posts <- recentFirst =<< loadAll "posts/*"

                let indexCtx =
                        listField "posts" postCtx (return (take 5 posts)) <>
                        defaultContext

                pandocCompiler
                    >>= applyAsTemplate indexCtx
                    >>= loadAndApplyTemplate "templates/default.html" indexCtx
                    >>= relativizeUrls
                

        match "templates/*" $ compile templateBodyCompiler

        create ["atom.xml"] $ do
            route idRoute
            compile $ do
                let feedCtx = postCtx <> bodyField "description"
                posts <- fmap (take 10) . recentFirst =<<
                    loadAllSnapshots "posts/*" "content"
                renderAtom myFeedConfiguration feedCtx posts

--------------------------------------------------------------------------------


myFeedConfiguration :: FeedConfiguration
myFeedConfiguration = FeedConfiguration
    { feedTitle       = "RJ's very own sick haskell blog"
    , feedDescription = "Your primer math shitposting solution."
    , feedAuthorName  = "Raphael Jacobs"
    , feedAuthorEmail = "raphy@airmail.cc"
    , feedRoot        = "https://raphyjake.gitlab.io"
    }

--------------------------------------------------------------------------------

postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y"  <>
    defaultContext

postCtxTags :: Tags -> Context String
postCtxTags tags = mconcat
    [ dateField "date" "%B %e, %Y"
    , tagsField "tags" tags
    , defaultContext
    ]
