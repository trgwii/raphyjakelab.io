---
title: Stop Having Fun.
theme: Seriously?
---

# No-fun manifesto

> "If a book makes me laugh I throw it in the trash.
> "Funny" is the lowest form of entertainment."
>        -  9695281

Having "fun" ruined your life in ways you can't even imagine. All the years you spent pursuing temporary pleasure were wasted. Now you're miserable. To avoid this, you should do the only thing you never wanted to: seek misery and boredom.
This is not some self-advice crap. I won't tell you to clean your room. This is my personal philosophy.
If you are completely satisfied and done with your current life, if you have no further dreams or aspirations to accomplish, well done. You did it, you might as well have fun. Stop reading right now. Otherwise, brace yourself.

-------------------------------------------------------------------------------------------

# Chapter 1: How "fun" ruined your life
>"Having fun isn't fun"

We live in a time where, in the western world, we have access to endless entertainment. We don't know what being "bored" feels like anymore. Have you ever been one month without internet or television? Did you ever end up binge-watching hours of videos from some random youtube channel you just discovered? Have you spent more than 3000 hours on a video game? Do you use social medias 10 hours a day?


I want you to think very hard of the last time you felt bored and had to actually come up with something. Think about it, when did it last happen? The answer is NEVER, or so long ago that you forgot. You've always had the radio, the television, or even worse, the internet, which gives you full, unrestricted access to all of mankind's knowledge. 


You are perfectly aware of this, of all the things you could do and all the people you could become. Sometimes you think to yourself, "oh my, I waste so much time! I could really start doing that thing I always wanted to!", but you NEVER do it. You blame the fact that you're a "procrastinator" or something.


Let's face reality: it's just NOT worth it. Why would you ever lift your sweaty ass from the couch?  You are happy this way, and anything else just takes too much effort for the same reward.
This is how FUN ruined your life. This is how it got in the way of realizing all your dreams and aspirations. You need to change your definition of "fun" and "boring", now.

-------------------------------------------------------------------------------------------

# Chapter 2: The case against hedonism
>"Every happy man is a failure."

Some argue that having FUN is the whole essence of life. Existence is a struggle, and so they seek escapism through FUN, which is widely available and accessible. After all, it all ends one day, and if you have only one shot at life you better enjoy it by avoiding all that causes pain and all that could lead to failure. Get yourself comfortable in your routine and habits, and never change. You like this, and it might as well go on forever.

If this is all you truly want to do, and all you truly aspire to, do it by all means. You should've stopped reading by now.

But never lie to yourself. You can't do it anyway. It is true that you don't have time to waste. It's my entire point: you can't waste time having fun if it's not what you really want to do. The clock is ticking, and nothingness awaits. It's been said that philosophy is _meditatio mori_, a way to prepare yourself for death. It is ultimately a thing every man has to deal with. I want you to think about this harder than ever. I want you to be terribly aware of each and every second that slips away from your life. May this feeling of existential dread never leave you. It will be your fuel. Do you truly want to have FUN now? Go ahead if you think so, I mean it. But be sure of your decision.

It takes very short to learn a new language or an instrument. By short, I mean a few years. Four years, six years, whatever. It's nothing, your life will be several decades long. Remember how long school was? Four years is nothing compared to that. You have all the time to learn a new language. It takes at most an hour every day if you're very serious about it.

Apply this reasoning to anything you want to learn, or to working out, absolutely anything. It might take a few years, but a few years is actually a SHORT time if you forget about all the instant gratification that mainstream entertainment provides.
Years of daily effort seems to much? Stop having fun.

-------------------------------------------------------------------------------------------

# Chapter 3: Ode to Boredom, or the Burden of being Immortal
>"Happiness was never important. The problem is that we don't know what we really want. What makes us happy is not to get what we want. But to dream about it. Happiness is for opportunists. So I think that the only life of deep satisfaction is a life of eternal struggle, especially struggle with oneself. If you want to remain happy, just remain stupid. Authentic masters are never happy; happiness is a category of slaves."
>
> -Slavoj Žižek, internet famous

There is a short story by Borges titled "El inmortal", where a man seeks a miraculous spring that grants immortality to whoever drinks it, and a nearby city of immortals. He eventually finds the river and the immortals, which left the city to live in caves. The immortals are not a very cheerful bunch, for they know that eventually, "all things happen to all men", they will write every book, they will visit any place... you get the idea: all their actions are insignificant. They will commit every kindness and every evil, they know no merit, there is no triumph in creating, for they're not individuals anymore, each one of them IS all men.

The immortals are terribly, unimaginably, utterly bored.

But here's what: you're mortal! You are terribly mortal. You're dying right now. And yet, you feel bored. You, before the universe in all its mirth and glory, still find the boldness to say "Meh, this is pretty boring!". 

This is not, and was never meant to be, a "productivity" tutorial. This isn't about getting productive at all, it is related to it at best. The whole point of "stop having fun" is to get back that thing called "long time commitment" that was taken away from us by the F.U.N. corporation.  Feeling like you have a short attention span? Stop having fun, or take ADHD medication. Feel like you have a million things you want to do, but never start doing them? Stop having fun, or take ADHD medication. Will it make you more productive, will it help your "work performance" or something, will your boss give you a raise if you stop having fun and work 80 hours a week? Maybe, probably not. Maybe you'll feel terrible. Foolish of you, to trust what a stranger wrote online. 

The truth is, your definition of fun must change. If you stop having fun, it will. You'll face boredom, your greatest friend and enemy. I highly suggest staring at a wall for two hours straight. Do it big: go for four. A wonderful exercise in willpower and a life-changing experience. You'll want to do SOMETHING, ANYTHING. You'll look around you, and think of all the things you left undone. And slowly, you will crawl to your garage and change that lightbulb, you'll reply to that email, you'll start to finally write your Manifesto, and most importantly you'll realize how much time you truly have.

I want you to feel bored AND mortal. You're bored, and today will never happen again, so you better make good use of it. Don't "kill time", time is already killing himself and he's taking you with him.
 
-------------------------------------------------------------------------------------------

# Chapter 4: The ramblings of an old man
>"Was it all a joke or not? No one will ever know."

As previously stated, this is my very own philosophy. I, the Author, believe every single word of this. The single best decision of my life was to stop having fun. To confuse the readers, I exaggerated a lot of passages to the point they're almost comical, including this bit. Nevertheless, buried deep within everything I wrote still lies a shard of truth, whatever truth means. Regardless, I don't care if my message is appreciated or not. It is up to the reader to interpret this text the way he wishes, to completely hate it and do the opposite of what I say, or to take it very seriously and become a true entertainment-less modern monk, like me.

I had a lot of fun writing this.