---
title: Software I use.
theme: Hammer without a master
---

This is just a personal list of software that I enjoy using.

This list will be frequently updated. Please, send me any suggestion you have. Contact info is on the homepage.

Last update: 21/04/2021

------------------------------------------------------------------------------------------

# [Operating Systems](#os){#os}

## [Void Linux](https://voidlinux.org/)

I haven't done a lot of distro hopping in my life, but I've had an overwhelmingly positive experience with Void Linux. 

I wouldn't recommend it to first time Linux users, not because it's particularly hard to use, but just because some things require reading skills (i.e. you have to make partitions manually, and you'll probably need a tutorial if it's the first time), but if you have some experience with Linux or alternatively have time to read the documentation, Void is a very comfortable disto for desktop usage. 

I've been using the same install for three years, and despite being a rolling release distro and despite me not updating very frequently it never broke, on contrary to the popular belief that rolling release == inevitable disasters. Another cool thing about void is that it keeps old kernels as long as you don't remove them, which means that if something breaks with the newest Linux kernel, you can always rollback painlessly to the one you were previously using until that something gets fixed.

I'm also one of those people who really enjoy being wholly in control of their system for some reason, and Void notably uses [runit](https://man.voidlinux.org/runit.8) as system supervisor, instead of the much more complex systemd. It also ships very barebones images from which you can install exactly the software you want.

------------------------------------------------------------------------------------------

# [Shell](#shell){#shell}


## [micro](https://micro-editor.github.io/)

Micro is a terminal editor that sits somewhat in between of nano and emacs/vim: it's not a full fledged IDE, yet it has syntax highlighting, a rather powerful plugin system (scriptable in lua), and very intuitive defaults. 

Copy-paste and mouse support both work out of the box, just like in your favourite graphical editor. It has an help page with a brief tutorial, and you can read the whole thing in less than 10 minutes. When you're lost, you can type the command "help defaultkeys" to get a quick recap of all the keybindings you might need. 
This makes micro very suitable for beginners, too. 

For more advanced users, Micro is very customizable, and allows to open multiple files, record macros, split the screen, and even open a terminal emulator inside of micro.

I use Micro whenever I want to quickly edit a file from console, or whenever I'm doing some small project and I can get away with just splitting the screen, writing in one page and compiling via command line in the other.

It's important to note that Micro also offers linting for some languages, such as C.

If you need a Nano replacement, you should definitely check Micro out. (That's where the name comes from!)

-------------------------------------------------------------------------------------------

## [Fish](https://fishshell.com/)


Fish is a shell which focuses on being user friendly. I really miss Fish when I'm not using it. You should just check it out, or read the features from their website.

Fish will parse manpages to generate autosuggestions for every program, and they'll pop up as you type.

If I type `rsync -` and press tab, Fish will tell me all the command line options rsync has, together with what they do. This works for any program that has a manpage.

Some people argue that Fish is bad and evil because it's not Posix. That is true, and indeed you can't copy paste any bash command you find online. Bash scrips won't run correctly with Fish, and it's generally advised to not set Fish as the default, system-wise shell. We have Dash for that. 

But this is hardly ever a problem. First of all, shebangs exists, and secondly, fish is meant to be, above all, a user-friendly interactive shell.

You can read the entire Fish manual in 30 minutes, and writing Fish scripts is not that insanely difficult if you know bash already. Regardless, you can still write your scripts in bash, and only use Fish interactively. That's what I do.

-------------------------------------------------------------------------------------------

## [z](https://github.com/rupa/z)

`z` is a command line utility to quickly jump around your most frequently visited folders.

Right after you install it, z is completely useless. But after moving around in a few directories, z will keep note of them, and then you'll be able to jump back inside instantly by typing `z keyword`, where `keyword` is any part of the directory you're trying to move to.

For instance. if I type `z down`, `z` will take me to the `raphy/Downloads` folder: it's a folder I open very frequently and it has the word `down` in it. Z does fuzzy-matching.

It doesn't sound too spectacular, but nowadays I really rely heavily on z. It feels a bit like magic: I can type `z proj` faster than I can think "Oh, I need to go in the project folder right now..."

I really suggest trying it out. If you use Fish, here's the [fish plugin](https://github.com/jethrokuan/z).

-------------------------------------------------------------------------------------------

## [fzf](https://github.com/junegunn/fzf)

In the same vein as `z`, which can be thought as a "fuzzy-cd", fzf is a fuzzy everything. Check the webpage for some examples.  It's another one of those programs that radically changed the way I use the terminal. 

Besides just finding files, fzf can be used to browse all the current environment variables and the command history too.

Another unorthodox way I use fzf is to preview files right off the terminal. Sometimes I just don't remember where some piece of code is, and I quickly call fzf to peek inside files.

If Fish is your shell, here's the [fish plugin](https://github.com/PatrickF1/fzf.fish).

-------------------------------------------------------------------------------------------

## [bat](https://github.com/sharkdp/bat)

Bat is a fancier version of cat. That's basically it. It has syntax highlighting, git integration, and a few other fancy features.

By default it also works as a pager when the file is too big to fit inside one screen, but I personally disabled that. I use it when I want to peek inside a file, but also want to see syntax highlighted. Not really a life changing tool, but very nice overall. 

-------------------------------------------------------------------------------------------

## [ripgrep](https://github.com/BurntSushi/ripgrep)

Ripgrep is just like grep, but recursive, and [insanely fast](https://blog.burntsushi.net/ripgrep/). It also comes with some sane defaults, such as ignoring .gitignore and binary files. Quoting ripgrep's githup page, 

> In other words, use ripgrep if you like speed, filtering by default, fewer bugs and Unicode support.